#Tomasz Slawek DEC 2007 Audatex
#Update 27/10/2023

ORACLE_SID=`ps -ef|grep asm_smon| awk -F_ '{print $3}'| grep -v '^\s*$'`


echo Seting Database Enviroment to $ORACLE_SID
export ORAENV_ASK=NO

. oraenv

export ORACLE_BASE ORACLE_HOME ORACLE_SID ORACLE_TERM ORAENV_ASK ORAHOME ORASID
export ORAENV_ASK=YES
