


#Set prompt
PS1='[\u@\h($ORACLE_SID)\W]\$ '



alias rlwrap='rlwrap -pRed --histsize 100000'
alias asm_space='~/dba_scripts/asm_space.sh'
alias asmcmd='rlwrap asmcmd'
alias adrci='rlwrap adrci'
alias dgmgrl='rlwrap dgmgrl /'
alias lsnrctl='rlwrap lsnrctl'
alias oh='cd $ORACLE_HOME;pwd'
alias rman='rlwrap rman'
alias smon='ps -ef | grep smon'
alias sqlplus='rlwrap  -if ~/dba_scripts/rlwrap_sqlplus_commands.list --histsize 100000 sqlplus'
alias ss='sqlplus / as sysdba'
alias s='sqlplus /nolog'
alias sas='sqlplus / as sysasm'
