#!/bin/bash
#
# Lists distinct oracle database session 
# session_list_by_machine.sh
# Tomasz Slawek 18 march 2006
#


${ORACLE_HOME}/bin/sqlplus -s /nolog <<EOF
set time off
set timing off
set feed off
set pages 10000
set lin 200

connect / as sysdba

----------------------------------------SQL-----------------------------------
select distinct machine,module from v\$session order by 1;
----------------------------------------SQL-----------------------------------
EOF

exit
